package com.chess;

import com.chess.engine.classic.board.Board;
import com.chess.engine.classic.board.BoardUtils;
import com.chess.engine.classic.board.Move.MoveFactory;
import com.chess.engine.classic.board.MoveTransition;
import com.chess.pgn.FenUtilities;

public class BlackWidow {

    public static void main(final String args[]) throws Exception {
        //Table.get().show();

        testSimpleEvaluation();


    }

//    public void kiwiPeteDepth1() {
//        final Board board = FenUtilities.createGameFromFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
//        final MoveStrategy minMax = new MiniMax(1);
//        minMax.execute(board);
////        assertEquals(minMax.getNumBoardsEvaluated(), 48L);
//    }

    public static void testSimpleEvaluation() {
        final Board board = FenUtilities.createGameFromFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        final MoveTransition t1 = board.currentPlayer()
                .makeMove(MoveFactory.createMove(board, BoardUtils.INSTANCE.getCoordinateAtPosition("e2"),
                        BoardUtils.INSTANCE.getCoordinateAtPosition("e4")));

        System.out.println(FenUtilities.createFENFromGame(t1.getToBoard()));

        System.out.println(t1.getMoveStatus().isDone());

//        assertTrue(t1.getMoveStatus().isDone());
//        final MoveTransition t2 = t1.getToBoard()
//                .currentPlayer()
//                .makeMove(MoveFactory.createMove(t1.getToBoard(), BoardUtils.INSTANCE.getCoordinateAtPosition("e7"),
//                        BoardUtils.INSTANCE.getCoordinateAtPosition("e5")));


//        assertTrue(t2.getMoveStatus().isDone());
//        assertEquals(StandardBoardEvaluator.get().evaluate(t2.getToBoard(), 0), 0);
    }

}
